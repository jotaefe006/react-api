import React, { useEffect, useState} from 'react';
import axios from "axios";
import "bootstrap/dist/css/bootstrap.css";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSearch} from '@fortawesome/free-solid-svg-icons';

import './App.css';

function App() {
  const url = "https://jsonplaceholder.typicode.com/todos";
  const [todos,setTodos] = useState([]);
  const [tablaTodos, setTablaTodos] = useState([]);
  const [busqueda, setBusqueda] = useState("");


  const getData = async () => {
   await axios.get(url)
    .then( response => {
      const responseJSON =  response.data;
      setTodos(responseJSON);
      setTablaTodos(responseJSON);
    })
    .catch(error => {
      console.log(error);
    })
  }

  const handleChange = e => {
    setBusqueda(e.target.value);
    filtrar(busqueda);
  }

  const filtrar = (terminoBusqueda) => {
    var resultadoBusqueda = tablaTodos.filter((elemento) => {
      if(elemento.title.toString().toLowerCase().includes(terminoBusqueda.toLowerCase()) ||
      elemento.id.toString().toLowerCase().includes(terminoBusqueda.toLowerCase()) ||
      elemento.userId.toString().toLowerCase().includes(terminoBusqueda.toLowerCase())) {
        return elemento;
      }
    });

    setTodos(resultadoBusqueda);
  } 

  useEffect(() => {
    getData();
  }, [])
  return (
    <div className="App">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h2>TODOS</h2>
          </div>
          <div className="col-md-12">
            <div className="containerInput">
              <input
                placeholder='Busqueda...'
                className="form-control inputBuscar"
                onChange={handleChange}
                ></input>
                <button className="btn btn-success">
                  <FontAwesomeIcon icon={faSearch} />
                </button>
            </div>
          </div>
          <div className="col-md-12">      
            <div className="table-responsive">
              <table className="" >
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>UserId</th>
                    <th>Title</th>
                    <th>Completed</th>
                  </tr>
                </thead>
                <tbody>
                {!todos ? 'Cargando': 
                  todos.map((todo,index) => (
                    // return <li>{todo.title} {todo.completed ? '👍': '👎'}</li>
                    <tr key={index}>
                      <td>{todo.id}</td>
                      <td>{todo.userId}</td>
                      <td>{todo.title}</td>
                      <td>{todo.completed ? '👍':'👎'}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>      
    </div>
  );
}

export default App;
